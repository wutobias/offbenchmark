# OFF xtalmd benchmark

### Description

This repo is mainly for sharing data with Yu-Tang and others.

`OFFBenchmark/{sage,parsley}_xml`: system xml files and pdb structures for each xtal based on the cif file xtal structure. These are the starting structures used as input for all subsequent minimization operations. Additionally, there is a system xml and pdb structure for each single monomer in each xtal. From the `.json` files, one can build an rdkit object of the xtal structure using `Chem.JSONToMols`. The csv files contain information about fractional positions and identifiers of each molecule in the supercell.

`OFFBenchmark/{sage,parsley}_min`: state xml files and pdb structures for each minimized xtal. The csv files contain the energies. The systems here were minimized using cell parameter and position gradients together.

`OFFBenchmark/{sage,parsley}_min_alternating`: state xml files and pdb structures for each minimized xtal. The csv files contain the energies. The systems here were minimized using cell parameter and position minimizations in alternating fashion.

`OFFBenchmark/{sage,parsley}_min_combo`: state xml files and pdb structures for each minimized xtal. The csv files contain the energies. The systems here were minimized first using cell parameters and position minimizations in alternating fashion and then using both together.